import React, {Component} from "react";
import Card from "./Components/Card/Card";

class CardDeck {
    state = {
        cards: []
    }
    constructor() {
        const cards = [];
        const suitArray = ['C', 'H', 'D', 'S'];

        for (let i = 0; i < 4; i++) {
            const suit = suitArray[i];

            for (let j = 0; j < 13; j++) {
                let rank = j + 2;

                if (j === 9) {
                    cards.push({
                        rank: 'J',
                        suit: suit
                    });
                } else if (j === 10) {
                    cards.push({
                        rank: 'Q',
                        suit: suit
                    });
                } else if (j === 11) {
                    cards.push({
                        rank: 'K',
                        suit: suit
                    });
                } else if (j === 12) {
                    cards.push({
                        rank: 'A',
                        suit: suit
                    });
                } else {
                    cards.push({
                        rank: rank,
                        suit: suit
                    });
                }
            }
        }
        this.cards = cards;
    }

    getCard() {
        const randomNumber = Math.floor(Math.random() * (this.cards.length));
        const [cardRandom] = this.cards.splice(randomNumber, 1);
        return cardRandom;
    }

    getCards(howMany) {
        const cardDeckArray = [];
        for (let i = 0; i < howMany; i++) {
            cardDeckArray.push(this.getCard());
        }
        return cardDeckArray;
    }
}

const cardsDeck = new CardDeck();
const cardsRandomFive = cardsDeck.getCards(5);
cardsDeck.state = cardsRandomFive;

const t = () => {
    for (let i = 0; i < cardsDeck.state.cards.length; i++) {
        return <Card rank={cardsDeck.state[i].rank} suit={cardsDeck.state[i].suit}/>;
    }
}

class App extends Component {
    render() {
        return (
            <div>
                <div className="playingCards fourColours rotateHand">
                    <div className="card back">*</div>
                    <div className="card back">*</div>
                    <div className="card back">*</div>
                    <div className="card back">*</div>
                    <div className="card back">*</div>
                    <Card rank={cardsDeck.state[0].rank} suit={cardsDeck.state[0].suit}/>
                    <Card rank={cardsDeck.state[1].rank} suit={cardsDeck.state[1].suit}/>
                    <Card rank={cardsDeck.state[2].rank} suit={cardsDeck.state[2].suit}/>
                    <Card rank={cardsDeck.state[3].rank} suit={cardsDeck.state[3].suit}/>
                    <Card rank={cardsDeck.state[4].rank} suit={cardsDeck.state[4].suit}/>
                </div>
                <div>
                    <button onClick={t}>Generate card</button>
                </div>
            </div>
        );
    }
}
export default App;
