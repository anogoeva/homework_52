import React from 'react';
import './Card.css'

const Card = props => {
    let suitName = '';
    let suitSymbol = '';
    if (props.suit === 'D') {
        suitName = "♦";
        suitSymbol = 'diams';
    } else if (props.suit === 'H') {
        suitName = '♥';
        suitSymbol = 'hearts'
    } else if (props.suit === 'C') {
        suitName = '♣';
        suitSymbol = 'clubs';
    } else {
        suitName = '♠';
        suitSymbol = 'spades';
    }
    let className = 'card rank-' + props.rank + ' ' + suitSymbol;
    return (
        <div className={className}>
            <span className="rank">{props.rank}</span>
            <span className="suit">{suitName}</span>
        </div>
    );
};
export default Card;